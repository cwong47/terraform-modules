variable "s3_state_bucket" {
  description = "S3 bucket for backend state"
}

variable "dynamo_lock_table" {
  description = "DynamoDB lock table name"
}

variable "tags" {
  type    = map(any)
  default = {}
}
