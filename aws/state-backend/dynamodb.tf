resource "aws_dynamodb_table" "terraform_locks" {
  name         = var.dynamo_lock_table
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }

  tags = var.tags
}
