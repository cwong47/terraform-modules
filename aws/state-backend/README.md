# AWS Backend State Terraform module

Terraform module which creates backend state.

## Usage

### Setup Backend State with S3/DynamoDB

```hcl
variable "s3_state_bucket" {
  default     = "company-qa-terraform-state"
}

variable "dynamo_state_lock_table" {
  default     = "company-state-lock-table"
}

module "terraform_state_backend" {
  source            = "../../modules/state-backend"
  s3_state_bucket   = var.s3_state_bucket
  dynamo_lock_table = var.dynamo_state_lock_table
}
```

## Resources

| Name | Type |
|------|------|
| [aws_dynamodb_table](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dynamodb_table) | resource |
| [aws_s3_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| `s3_state_bucket` | S3 bucket name. | `string` | `null` | yes |
| `dynamo_lock_table` | DynamoDB table name. | `string` | `null` | yes |
| `tags` | A map of tags to assign to the resource. | `map(any)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| `s3_state_bucket_arn` | The ARN of the S3 bucket. |
| `dynamodb_state_table_name` | The name of the DynamoDB table. |
