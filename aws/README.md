# AWS Terraform Modules

- backend state using S3/DynamoDB
- Application Load Balancer
- ECS cluster for Fargate
- ECS service (ALB)
- ECS service

## Requirement

Terraform >= 1.1.3

I recommend managing your Terraform version via [tfenv](https://github.com/tfutils/tfenv).

## Usage

Usages are in the README.md of each module.
