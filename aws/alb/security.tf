resource "aws_security_group" "alb" {
  name        = var.name
  description = "${var.name} ALB security group"
  vpc_id      = var.vpc_id
  tags        = var.tags
}

resource "aws_security_group_rule" "alb_ingress_80" {
  count             = var.internal == false ? 1 : 0
  type              = "ingress"
  from_port         = "80"
  to_port           = "80"
  protocol          = "tcp"
  cidr_blocks       = var.allowed_cidr
  security_group_id = aws_security_group.alb.id
}

resource "aws_security_group_rule" "alb_ingress_443" {
  count             = var.internal == false ? 1 : 0
  type              = "ingress"
  from_port         = "443"
  to_port           = "443"
  protocol          = "tcp"
  cidr_blocks       = var.allowed_cidr
  security_group_id = aws_security_group.alb.id
}

resource "aws_security_group_rule" "alb_egress" {
  type      = "egress"
  from_port = 0
  to_port   = 0
  protocol  = "-1"

  cidr_blocks = [
    "0.0.0.0/0",
  ]

  security_group_id = aws_security_group.alb.id
}

resource "aws_security_group_rule" "alb_internal_ingress_80" {
  count     = var.internal == true ? 1 : 0
  type      = "ingress"
  from_port = "80"
  to_port   = "80"
  protocol  = "tcp"

  cidr_blocks = [
    var.vpc_cidr,
  ]

  security_group_id = aws_security_group.alb.id
}

resource "aws_security_group_rule" "alb_internal_ingress_443" {
  count     = var.internal == true ? 1 : 0
  type      = "ingress"
  from_port = "443"
  to_port   = "443"
  protocol  = "tcp"

  cidr_blocks = [
    var.vpc_cidr,
  ]

  security_group_id = aws_security_group.alb.id
}
