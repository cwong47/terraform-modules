output "alb_id" {
  value = aws_lb.alb.id
}

output "alb_arn" {
  value = aws_lb.alb.arn
}

output "zone_id" {
  value = aws_lb.alb.zone_id
}

output "dns_name" {
  value = aws_lb.alb.dns_name
}

output "security_group_id" {
  value = aws_security_group.alb.id
}

output "listener_http" {
  value = aws_lb_listener.alb_http_fixed_response.id
}

output "listener_https" {
  value = aws_lb_listener.alb_https_fixed_response.id
}
