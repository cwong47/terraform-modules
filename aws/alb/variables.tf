variable "name" {
  description = "The ALB name"
}

variable "vpc_id" {
  description = "The VPC id"
}

variable "vpc_cidr" {
  description = "The VPC CIDR"
  default     = ""
}

variable "subnet_ids" {
  type        = list(any)
  description = "The subnets for the ALB"
}

variable "allowed_cidr" {
  type        = list(any)
  description = "The CIDR blocks account_id with the service"
  default     = ["0.0.0.0/0"]
}

variable "internal" {
  description = "Should the ALB be internal only"
  default     = false
}

variable "access_logs" {
  description = "Boolean to enable / disable access_log"
  default     = false
}

variable "access_logs_bucket" {
  description = "The S3 bucket to store access logs in"
}

variable "access_logs_prefix" {
  description = "The directory to store access logs in"
  default     = "alb_logs"
}

variable "https_certificate_arn" {
  description = "A full ARN path to the ACM SSL certificate for the ALB"
  default     = false
}

variable "https_ssl_policy" {
  default = "ELBSecurityPolicy-FS-1-2-Res-2019-08"
}

variable "tags" {
  type    = map(any)
  default = {}
}
