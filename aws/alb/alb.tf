resource "aws_lb" "alb" {
  name               = var.name
  internal           = var.internal
  load_balancer_type = "application"
  security_groups    = flatten([aws_security_group.alb.id])
  subnets            = flatten([var.subnet_ids])

  access_logs {
    bucket  = var.access_logs_bucket
    prefix  = var.access_logs_prefix
    enabled = var.access_logs
  }

  idle_timeout    = 120
  enable_http2    = true
  ip_address_type = "ipv4"

  tags = var.tags
}

resource "aws_lb_listener" "alb_http_fixed_response" {
  load_balancer_arn = aws_lb.alb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "If you see this, the ALB target group (${var.name}) is not properly configured or is not working."
      status_code  = "503"
    }
  }
}

resource "aws_lb_listener" "alb_https_fixed_response" {
  load_balancer_arn = aws_lb.alb.arn
  port              = 443
  protocol          = "HTTPS"
  ssl_policy        = var.https_ssl_policy
  certificate_arn   = var.https_certificate_arn

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "If you see this, the ALB target group (${var.name}) is not properly configured or is not working."
      status_code  = "503"
    }
  }
}
