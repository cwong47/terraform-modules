# AWS Application Load Balancer (ALB) Terraform module

Terraform module which creates AWS Application Load Balaner.

## Usage

### ALB with SSL certificate

```hcl
module "alb_rest_service_blue" {
  source                = "../../modules/alb"
  name                  = "${var.environment}-app-name-01"
  vpc_id                = data.aws_vpc.usw2.id
  subnet_ids            = [data.aws_subnet_ids.usw2_public.ids]
  access_logs_bucket    = aws_s3_bucket.alb_access_logs.id
  https_certificate_arn = var.https_certificate_arn
}
```

## Resources

| Name | Type |
|------|------|
| [aws_lb_listener](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb_listener) | resource |
| [aws_lb](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb) | resource |
| [aws_security_group_rule](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| `name` | ALB name. | `string` | `null` | yes |
| `vpc_id` | VPC ID. | `string` | `null` | yes |
| `vpc_cidr` | VPC CIDR. | `string` | `null` | no |
| `subnet_ids` | List of subnets for the ALB. | `list(any)` | `null` | yes |
| `allowed_cidr` | The CIDR blocks account_id with the service. | `list(any)` | `["0.0.0.0/0"]` | no |
| `internal` | Should the ALB be internal only. | `bool` | `false` | no |
| `access_logs` | Boolean to enable / disable access_log. | `bool` | `false` | no |
| `access_logs_bucket` | The S3 bucket to store access logs in. | `string` | `null` | yes |
| `access_logs_prefix` | The directory to store access logs in. | `bool` | `false` | no |
| `https_certificate_arn` | A full ARN path to the ACM SSL certificate for the ALB. | `any` | `false` | no |
| `https_ssl_policy` | AWS Load Balancer security policy. | `string` | `ELBSecurityPolicy-FS-1-2-Res-2019-08` | no |
| `tags` | A map of tags to assign to the resource. | `map(any)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| `alb_id` | ALB ID. |
| `alb_arn` | ALB ARN. |
| `zone_id` | DNS zone ID. |
| `dns_name` | DNS name. |
| `security_group_id` | Security group ID. |
| `listener_http` | HTTP listener. |
| `listener_https` | HTTPS listener. |
