resource "aws_security_group" "container" {
  name        = "${var.environment}-${var.service_name}-${var.service_cluster}-container"
  description = "${var.environment} container security group"
  vpc_id      = var.vpc_id
  tags        = var.tags
}

resource "aws_security_group_rule" "container_ingress" {
  type                     = "ingress"
  from_port                = var.ingress_from_port
  to_port                  = var.ingress_to_port
  protocol                 = "tcp"
  security_group_id        = aws_security_group.container.id
  source_security_group_id = var.source_security_group_id
}

resource "aws_security_group_rule" "container_egress" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1" // All protocols
  security_group_id = aws_security_group.container.id

  cidr_blocks = [
    "0.0.0.0/0",
  ]
}
