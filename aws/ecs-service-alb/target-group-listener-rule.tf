resource "aws_lb_target_group" "alb_be_http" {
  name                 = "${var.environment}-${var.service_name}-${var.service_cluster}"
  port                 = var.alb_port_http
  protocol             = "HTTP"
  vpc_id               = var.vpc_id
  target_type          = "ip"
  deregistration_delay = var.deregistration_delay

  stickiness {
    type    = "lb_cookie"
    enabled = false
  }

  health_check {
    interval            = var.health_check_interval
    protocol            = "HTTP"
    path                = var.health_check_path
    healthy_threshold   = 3
    unhealthy_threshold = 3
    matcher             = var.health_check_status_code_expect
    timeout             = var.health_check_timeout
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_lb_listener_rule" "alb_http_redirect" {
  count        = var.alb_listener_http != "" && length(var.path_patterns_http) == 0 ? 1 : 0
  listener_arn = var.alb_listener_http

  action {
    type = "redirect"

    redirect {
      protocol    = "HTTPS"
      port        = "443"
      status_code = "HTTP_301"
    }
  }

  condition {
    path_pattern {
      values = ["/*"]
    }
  }
}

resource "aws_lb_listener_rule" "alb_http_forward" {
  count        = length(var.path_patterns_http) == 0 ? 0 : 1
  listener_arn = var.alb_listener_http
  priority     = element(values(var.path_patterns_http), count.index)

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.alb_be_http.arn
  }

  condition {
    path_pattern {
      values = [element(keys(var.path_patterns_http), count.index)]
    }
  }
}

resource "aws_lb_listener_rule" "alb_https_forward" {
  count        = length(var.path_patterns_https)
  listener_arn = var.alb_listener_https
  priority     = element(values(var.path_patterns_https), count.index)

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.alb_be_http.arn
  }

  condition {
    path_pattern {
      values = [element(keys(var.path_patterns_https), count.index)]
    }
  }
}
