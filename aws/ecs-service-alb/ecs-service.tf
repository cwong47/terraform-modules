data "external" "get_image_tag" {
  program = ["bash", "${path.module}/scripts/get-image-tag.sh"]

  query = {
    cluster_name = var.cluster_name
    service_name = "${var.environment}-${var.service_name}-${var.service_cluster}"
    region       = var.region
  }
}

resource "aws_ecs_service" "service" {
  count                              = var.service_registry_arn == "" ? 1 : 0
  name                               = "${var.environment}-${var.service_name}-${var.service_cluster}"
  task_definition                    = "${aws_ecs_task_definition.definition.family}:${max(aws_ecs_task_definition.definition.revision, data.aws_ecs_task_definition.definition.revision)}"
  desired_count                      = var.desired_count
  launch_type                        = var.launch_type
  platform_version                   = var.platform_version
  scheduling_strategy                = "REPLICA"
  cluster                            = var.cluster_arn
  deployment_minimum_healthy_percent = var.deployment_minimum_healthy_percent
  deployment_maximum_percent         = var.deployment_maximum_percent
  health_check_grace_period_seconds  = var.health_check_grace_period_seconds
  enable_ecs_managed_tags            = true
  enable_execute_command             = var.exec_command
  propagate_tags                     = "SERVICE"

  deployment_controller {
    type = "ECS"
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.alb_be_http.arn
    container_name   = var.container_name
    container_port   = var.container_port
  }

  network_configuration {
    subnets          = flatten([var.subnet_ids])
    security_groups  = length(var.allowed_security_groups) != 0 ? flatten([join(", ", var.allowed_security_groups), aws_security_group.container.id]) : flatten([aws_security_group.container.id])
    assign_public_ip = var.assign_public_ip
  }

  tags = var.tags
}

resource "aws_ecs_service" "service_with_discovery" {
  count                              = var.service_registry_arn == "" ? 0 : 1
  name                               = "${var.environment}-${var.service_name}-${var.service_cluster}"
  task_definition                    = "${aws_ecs_task_definition.definition.family}:${max(aws_ecs_task_definition.definition.revision, data.aws_ecs_task_definition.definition.revision)}"
  desired_count                      = var.desired_count
  launch_type                        = var.launch_type
  platform_version                   = var.platform_version
  scheduling_strategy                = "REPLICA"
  cluster                            = var.cluster_arn
  deployment_minimum_healthy_percent = var.deployment_minimum_healthy_percent
  deployment_maximum_percent         = var.deployment_maximum_percent
  health_check_grace_period_seconds  = var.health_check_grace_period_seconds
  enable_ecs_managed_tags            = true
  enable_execute_command             = var.exec_command
  propagate_tags                     = "SERVICE"

  deployment_controller {
    type = "ECS"
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.alb_be_http.arn
    container_name   = var.container_name
    container_port   = var.container_port
  }

  network_configuration {
    subnets          = flatten([var.subnet_ids])
    security_groups  = flatten([aws_security_group.container.id])
    assign_public_ip = var.assign_public_ip
  }

  tags = var.tags
  service_registries {
    registry_arn   = var.service_registry_arn
    container_name = var.container_name
    container_port = var.container_port
  }
}
