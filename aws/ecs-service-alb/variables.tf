variable "account_id" {
}

variable "service_name" {
  description = "The name of the service"
}

variable "service_cluster" {
  description = "The cluster name of the service"
}

variable "region" {
  default = "us-west-2"
}

variable "environment" {
}

variable "log_driver" {
  description = "Default log driver to use."
  default     = "awsfirelens"
}

variable "canary" {
  description = "Specify optional canary environment variable to ECS task definition, available to docker."
  default     = ""
}

variable "image_name" {
}

variable "image_tag" {
  description = "Docker tag for images retrieved from ECR or docker registries."
  default     = "latest"
}

variable "alb_port_http" {
  description = "Listening port on ALB, defaults to standard http 80"
  default     = "80"
}

variable "ingress_from_port" {
  description = "Listening port from, defaults to standard tcp 80"
  default     = "80"
}

variable "ingress_to_port" {
  description = "Listening port to, defaults to standard tcp 80"
  default     = "80"
}

variable "desired_count" {
  description = "The number of instances of the task definition to place and keep running"
}

variable "launch_type" {
  default     = "FARGATE"
  description = "The launch type on which to run your service. The valid values are EC2 and FARGATE"
}

variable "platform_version" {
  default     = "LATEST"
  description = "The version of FARGATE"
}

variable "cluster_name" {
  description = "Name of the ECS cluster to run the service in"
}

variable "cluster_arn" {
  description = "ARN of the ECS cluster to run the service in"
}

variable "exec_command" {
  default     = true
  description = "Manage ECS Execute Command"
}

variable "deployment_minimum_healthy_percent" {
  default     = 100
  description = "The lower limit (as a percentage of the service's desiredCount) of the number of running tasks that must remain running and healthy in a service during a deployment"
}

variable "deployment_maximum_percent" {
  default     = 200
  description = "The upper limit (as a percentage of the service's desiredCount) of the number of running tasks that can be running in a service during a deployment"
}

variable "health_check_grace_period_seconds" {
  default     = 0
  description = "Seconds to ignore failing load balancer health checks on newly instantiated tasks to prevent premature shutdown"
}

variable "container_name" {
  description = "The name of the container to associate with the load balancer (as it appears in a container definition)"
}

variable "container_port" {
  description = "The port on the container to associate with the load balancer"
}

variable "vpc_id" {
}

variable "subnet_ids" {
  type        = list(any)
  description = "The subnets associated with the service"
}

variable "source_security_group_id" {
}

variable "allowed_security_groups" {
  type        = list(string)
  description = "The security group ids that should be allowed unfiltered ingress to container instances"
  default     = []
}

variable "assign_public_ip" {
  default     = false
  description = "Assign a public IP address to the ENI"
}

variable "tags" {
  type    = map(any)
  default = {}
}

variable "deregistration_delay" {
  description = "The amount of time in seconds to drain requests when de-registering a target"
  default     = 60
}

variable "health_check_path" {
  description = "The path the health check hits"
  default     = "/"
}

variable "health_check_interval" {
  description = "Health check interval in seconds"
  default     = 15
}

variable "health_check_timeout" {
  description = "Health check timeout in seconds"
  default     = 14
}

variable "health_check_status_code_expect" {
  description = "Health check status code expected to return"
  default     = "200,301,302"
}

variable "load_balancer_arn" {
  default = ""
}

variable "alb_listener_http" {
  description = "Listener ARN"
  default     = ""
}

variable "alb_listener_https" {
  description = "Listener ARN"
  default     = ""
}

variable "path_patterns_http" {
  type        = map(string)
  description = "Paths and their respective priority (HTTP), to be used in the alb listener rule for this service"
  default     = {}
}

variable "path_patterns_https" {
  type        = map(string)
  description = "Paths and their respective priority (HTTPS), to be used in the alb listener rule for this service"
  default     = {}
}

################################
### Auto Scaling
################################

variable "autoscaling_min_count" {
  default     = "-1"
  description = "The minimum number of instances of the task definition to place and keep running"
}

variable "autoscaling_max_count" {
  default     = "-1"
  description = "The maximum number of instances of the task definition to place and keep running"
}

variable "autoscaling_aggregation_type" {
  default     = "Average"
  description = "The aggregation type for the policy's metrics."
}

variable "autoscaling_cooldown_down" {
  default     = "180"
  description = "The amount of time, in seconds, after a scaling activity completes and before the next scaling activity can start."
}

variable "autoscaling_interval_upper_bound_down" {
  default     = "0"
  description = "The upper bound for the difference between the alarm threshold and the CloudWatch metric."
}

variable "autoscaling_adjustment_down" {
  default     = "-1"
  description = "The number of members by which to scale, when the adjustment bounds are breached. A positive value scales up. A negative value scales down."
}

variable "autoscaling_cooldown_up" {
  default     = "60"
  description = "The amount of time, in seconds, after a scaling activity completes and before the next scaling activity can start."
}

variable "autoscaling_interval_upper_bound_up" {
  default     = "0"
  description = "The upper bound for the difference between the alarm threshold and the CloudWatch metric."
}

variable "autoscaling_adjustment_up" {
  default     = "1"
  description = "The number of members by which to scale, when the adjustment bounds are breached. A positive value scales up. A negative value scales down."
}

variable "autoscaling_low_cpu_threshold" {
  default     = "-1"
  description = "The CPU percent utilization threshold to autoscale down"
}

variable "autoscaling_high_cpu_threshold" {
  default     = "-1"
  description = "The CPU percent utilization threshold to autoscale up"
}

variable "autoscaling_low_memory_threshold" {
  default     = "-1"
  description = "The Memory percent utilization threshold to autoscale down"
}

variable "autoscaling_high_memory_threshold" {
  default     = "-1"
  description = "The Memory percent utilization threshold to autoscale up"
}

variable "autoscaling_low_rpm_threshold" {
  default     = "-1"
  description = "The RPM percent utilization threshold to autoscale down"
}

variable "autoscaling_high_rpm_threshold" {
  default     = "-1"
  description = "The RPM Memory percent utilization threshold to autoscale up"
}

################################
### Task Definition
################################

variable "container_definition" {
  default     = ""
  description = "A container definition provided as a single valid JSON document"
}

variable "task_role_name" {
  default     = ""
  description = "The name of IAM role that allows your Amazon ECS container task to make calls to other AWS services"
}

variable "task_role_arn" {
  default     = ""
  description = "The ARN of IAM role that allows your Amazon ECS container task to make calls to other AWS services"
}

variable "iam_policy_arn" {
  default     = ["arn:aws:iam::aws:policy/ReadOnlyAccess"]
  description = "IAM Policy to be attached to role"
  type        = list(any)
}

variable "network_mode" {
  default     = "awsvpc"
  description = "The Docker networking mode to use for the containers in the task"
}

variable "cpu" {
  description = "The number of cpu units used by the task"
}

variable "memory" {
  description = "The amount (in MiB) of memory used by the task"
}

variable "requires_compatibilities" {
  default     = ["FARGATE"]
  description = "A set of launch types required by the task"
  type        = list(any)
}

variable "service_registry_arn" {
  default     = ""
  description = "ARN of the Service Registry."
}

variable "volume" {
  default     = []
  description = "A set of volume blocks that containers in your task may use."
}
