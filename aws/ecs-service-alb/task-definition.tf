data "template_file" "definition" {
  template = var.container_definition != "" ? var.container_definition : file("${path.module}/task-definitions/application-${var.log_driver}.json")

  vars = {
    name           = var.service_name
    cluster        = var.service_cluster
    account_id     = var.account_id
    image_name     = var.image_name
    image_tag      = var.image_tag != "latest" ? var.image_tag : data.external.get_image_tag.result.image_tag
    container_port = var.container_port
    memory         = var.memory
    cpu            = var.cpu
    environment    = var.environment
    region         = var.region
    canary         = var.canary
  }
}
resource "aws_ecs_task_definition" "definition_seed" {
  family                   = "${var.environment}-${var.service_name}-${var.service_cluster}"
  container_definitions    = data.template_file.definition.rendered
  task_role_arn            = var.task_role_arn != "" ? var.task_role_arn : aws_iam_role.task_execution.arn
  execution_role_arn       = aws_iam_role.task_execution.arn
  network_mode             = var.network_mode
  cpu                      = var.cpu
  memory                   = var.memory
  requires_compatibilities = var.requires_compatibilities
  tags                     = var.tags

  dynamic "volume" {
    for_each = var.volume

    content {
      name = volume.value.name

      dynamic "efs_volume_configuration" {
        for_each = lookup(volume.value, "efs_volume_configuration", [])

        content {
          file_system_id          = lookup(efs_volume_configuration.value, "file_system_id", null)
          root_directory          = lookup(efs_volume_configuration.value, "root_directory", null)
          transit_encryption      = lookup(efs_volume_configuration.value, "transit_encryption", "ENABLED")
          transit_encryption_port = lookup(efs_volume_configuration.value, "transit_encryption_port", 2999)

          dynamic "authorization_config" {
            for_each = lookup(efs_volume_configuration.value, "authorization_config", [])

            content {
              access_point_id = lookup(authorization_config.value, "access_point_id", null)
              iam             = lookup(authorization_config.value, "iam", "ENABLED")
            }
          }
        }
      }
    }
  }

  lifecycle {
    ignore_changes = [container_definitions]
  }
}

data "aws_ecs_task_definition" "definition" {
  task_definition = aws_ecs_task_definition.definition.family
  depends_on      = [aws_ecs_task_definition.definition_seed]
}

resource "aws_ecs_task_definition" "definition" {
  family                   = "${var.environment}-${var.service_name}-${var.service_cluster}"
  container_definitions    = data.template_file.definition.rendered
  task_role_arn            = var.task_role_arn != "" ? var.task_role_arn : aws_iam_role.task_execution.arn
  execution_role_arn       = aws_iam_role.task_execution.arn
  network_mode             = var.network_mode
  cpu                      = var.cpu
  memory                   = var.memory
  requires_compatibilities = var.requires_compatibilities
  tags                     = var.tags

  dynamic "volume" {
    for_each = var.volume

    content {
      name = volume.value.name

      dynamic "efs_volume_configuration" {
        for_each = lookup(volume.value, "efs_volume_configuration", [])

        content {
          file_system_id          = lookup(efs_volume_configuration.value, "file_system_id", null)
          root_directory          = lookup(efs_volume_configuration.value, "root_directory", null)
          transit_encryption      = lookup(efs_volume_configuration.value, "transit_encryption", "ENABLED")
          transit_encryption_port = lookup(efs_volume_configuration.value, "transit_encryption_port", 2999)

          dynamic "authorization_config" {
            for_each = lookup(efs_volume_configuration.value, "authorization_config", [])

            content {
              access_point_id = lookup(authorization_config.value, "access_point_id", null)
              iam             = lookup(authorization_config.value, "iam", "ENABLED")
            }
          }
        }
      }
    }
  }
}
