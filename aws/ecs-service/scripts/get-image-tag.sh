#!/usr/bin/env bash

# jq reads from stdin so we don't have to set up any inputs, but let's validate the outputs
eval "$(jq -r '@sh "export CLUSTER_NAME=\(.cluster_name) SERVICE_NAME=\(.service_name) REGION=\(.region)"')"
[[ -z "${CLUSTER_NAME}" ]] && export CLUSTER_NAME=none
[[ -z "${SERVICE_NAME}" ]] && export SERVICE_NAME=none
[[ -z "${REGION}" ]] && export REGION=none

task_arn="$(aws --region $REGION ecs describe-services --cluster $CLUSTER_NAME --service $SERVICE_NAME 2> /dev/null | jq -r .services[0].taskDefinition)"
image=$(aws --region $REGION ecs describe-task-definition --task-def $task_arn 2> /dev/null | jq -r ".taskDefinition.containerDefinitions[] | select (.name == \"${SERVICE_NAME}\") | (.image | split(\":\")[1])")
echo "{ \"image_tag\": \"${image:-latest}\" }"
