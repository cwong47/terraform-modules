resource "aws_iam_role" "task_execution" {
  name = "${var.environment}-${var.service_name}-${var.service_cluster}-task-execution"

  assume_role_policy = <<-POLICY
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Sid": "",
        "Effect": "Allow",
        "Principal": {
          "Service": [
            "ecs.amazonaws.com",
            "ecs-tasks.amazonaws.com"
          ]
        },
        "Action": "sts:AssumeRole"
      }
    ]
  }
  POLICY
}

resource "aws_iam_role_policy" "task_execution" {
  name = "${var.environment}-${var.service_name}-${var.service_cluster}-task-execution"
  role = aws_iam_role.task_execution.id

  policy = <<-POLICY
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": [
          "cloudwatch:Describe*",
          "cloudwatch:Get*",
          "cloudwatch:List*",
          "ecr:BatchCheckLayerAvailability",
          "ecr:BatchGetImage",
          "ecr:GetAuthorizationToken",
          "ecr:GetDownloadUrlForLayer",
          "ecs:DiscoverPollEndpoint",
          "ecs:Poll",
          "ecs:StartTask",
          "ecs:Submit*",
          "kms:Decrypt",
          "kms:DescribeKey",
          "kms:Encrypt",
          "kms:GenerateDataKey*",
          "kms:ReEncrypt*",
          "logs:CreateLogGroup",
          "logs:CreateLogStream",
          "logs:Describe*",
          "logs:Get*",
          "logs:PutLogEvents",
          "ssm:Get*",
          "ssmmessages:CreateControlChannel",
          "ssmmessages:CreateDataChannel",
          "ssmmessages:OpenControlChannel",
          "ssmmessages:OpenDataChannel"
        ],
        "Resource": "*"
      }
    ]
  }
  POLICY
}

resource "aws_iam_role_policy_attachment" "role_policy_attachment" {
  count      = length(var.iam_policy_arn)
  role       = var.task_role_name != "" ? var.task_role_name : aws_iam_role.task_execution.name
  policy_arn = var.iam_policy_arn[count.index]
}
