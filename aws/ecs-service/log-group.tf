resource "aws_cloudwatch_log_group" "log_group" {
  name              = "/ecs/${var.environment}-${var.service_name}-${var.service_cluster}"
  retention_in_days = 30
  tags              = var.tags
}

resource "aws_cloudwatch_log_group" "log_group_datadog" {
  name              = "/ecs/${var.environment}-${var.service_name}-${var.service_cluster}-datadog"
  retention_in_days = 7
  tags              = var.tags
}
