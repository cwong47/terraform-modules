resource "aws_appautoscaling_target" "service_autoscaling" {
  count              = var.autoscaling_min_count == "-1" && var.autoscaling_max_count == "-1" ? 0 : 1
  min_capacity       = var.autoscaling_min_count
  max_capacity       = var.autoscaling_max_count
  resource_id        = "service/${var.cluster_name}/${length(aws_ecs_service.service) > 0 ? lookup(aws_ecs_service.service[count.index], "name") : lookup(aws_ecs_service.service_with_discovery[count.index], "name")}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

resource "aws_appautoscaling_policy" "service_autoscaling_down" {
  count              = var.autoscaling_min_count == "-1" && var.autoscaling_max_count == "-1" ? 0 : 1
  name               = "${var.environment}-${var.service_name}-${var.service_cluster}-scale-down"
  policy_type        = "StepScaling"
  resource_id        = aws_appautoscaling_target.service_autoscaling[count.index].resource_id
  scalable_dimension = aws_appautoscaling_target.service_autoscaling[count.index].scalable_dimension
  service_namespace  = aws_appautoscaling_target.service_autoscaling[count.index].service_namespace

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = var.autoscaling_cooldown_down
    metric_aggregation_type = var.autoscaling_aggregation_type

    step_adjustment {
      metric_interval_upper_bound = var.autoscaling_interval_upper_bound_down
      scaling_adjustment          = var.autoscaling_adjustment_down
    }
  }

  depends_on = [aws_appautoscaling_target.service_autoscaling]
}

resource "aws_appautoscaling_policy" "service_autoscaling_up" {
  count              = var.autoscaling_min_count == "-1" && var.autoscaling_max_count == "-1" ? 0 : 1
  name               = "${var.environment}-${var.service_name}-${var.service_cluster}-scale-up"
  policy_type        = "StepScaling"
  resource_id        = aws_appautoscaling_target.service_autoscaling[count.index].resource_id
  scalable_dimension = aws_appautoscaling_target.service_autoscaling[count.index].scalable_dimension
  service_namespace  = aws_appautoscaling_target.service_autoscaling[count.index].service_namespace

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = var.autoscaling_cooldown_up
    metric_aggregation_type = var.autoscaling_aggregation_type

    step_adjustment {
      metric_interval_lower_bound = var.autoscaling_interval_upper_bound_up
      scaling_adjustment          = var.autoscaling_adjustment_up
    }
  }

  depends_on = [aws_appautoscaling_target.service_autoscaling]
}

resource "aws_cloudwatch_metric_alarm" "cpu_utilization_low" {
  count               = var.autoscaling_low_cpu_threshold == "-1" ? 0 : 1
  alarm_name          = "${var.cluster_name}-${var.environment}-${var.service_name}-${var.service_cluster}-cpu-low-${var.autoscaling_low_cpu_threshold}"
  alarm_description   = "Managed by Terraform"
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ECS"
  period              = "60"
  statistic           = "Average"
  threshold           = var.autoscaling_low_cpu_threshold

  dimensions = {
    ClusterName = var.cluster_name
    ServiceName = length(aws_ecs_service.service) > 0 ? lookup(aws_ecs_service.service[count.index], "name") : lookup(aws_ecs_service.service_with_discovery[count.index], "name")
  }

  alarm_actions = flatten([aws_appautoscaling_policy.service_autoscaling_down[count.index].arn])
}

resource "aws_cloudwatch_metric_alarm" "cpu_utilization_high" {
  count               = var.autoscaling_high_cpu_threshold == "-1" ? 0 : 1
  alarm_name          = "${var.cluster_name}-${var.environment}-${var.service_name}-${var.service_cluster}-cpu-high-${var.autoscaling_high_cpu_threshold}"
  alarm_description   = "Managed by Terraform"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ECS"
  period              = "60"
  statistic           = "Average"
  threshold           = var.autoscaling_high_cpu_threshold

  dimensions = {
    ClusterName = var.cluster_name
    ServiceName = length(aws_ecs_service.service) > 0 ? lookup(aws_ecs_service.service[count.index], "name") : lookup(aws_ecs_service.service_with_discovery[count.index], "name")
  }

  alarm_actions = flatten([aws_appautoscaling_policy.service_autoscaling_up[count.index].arn])
}

resource "aws_cloudwatch_metric_alarm" "memory_utilization_low" {
  count               = var.autoscaling_low_memory_threshold == "-1" ? 0 : 1
  alarm_name          = "${var.cluster_name}-${var.environment}-${var.service_name}-${var.service_cluster}-memory-low-${var.autoscaling_low_memory_threshold}"
  alarm_description   = "Managed by Terraform"
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "MemoryUtilization"
  namespace           = "AWS/ECS"
  period              = "60"
  statistic           = "Average"
  threshold           = var.autoscaling_low_memory_threshold

  dimensions = {
    ClusterName = var.cluster_name
    ServiceName = length(aws_ecs_service.service) > 0 ? lookup(aws_ecs_service.service[count.index], "name") : lookup(aws_ecs_service.service_with_discovery[count.index], "name")
  }

  alarm_actions = flatten([aws_appautoscaling_policy.service_autoscaling_down[count.index].arn])
}

resource "aws_cloudwatch_metric_alarm" "memory_utilization_high" {
  count               = var.autoscaling_high_memory_threshold == "-1" ? 0 : 1
  alarm_name          = "${var.cluster_name}-${var.environment}-${var.service_name}-${var.service_cluster}-memory-high-${var.autoscaling_high_memory_threshold}"
  alarm_description   = "Managed by Terraform"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "MemoryUtilization"
  namespace           = "AWS/ECS"
  period              = "60"
  statistic           = "Average"
  threshold           = var.autoscaling_high_memory_threshold

  dimensions = {
    ClusterName = var.cluster_name
    ServiceName = length(aws_ecs_service.service) > 0 ? lookup(aws_ecs_service.service[count.index], "name") : lookup(aws_ecs_service.service_with_discovery[count.index], "name")
  }

  alarm_actions = flatten([aws_appautoscaling_policy.service_autoscaling_up[count.index].arn])
}

resource "aws_cloudwatch_metric_alarm" "rpm_utilization_low" {
  count               = var.autoscaling_low_rpm_threshold == "-1" ? 0 : 1
  alarm_name          = "${var.cluster_name}-${var.environment}-${var.service_name}-${var.service_cluster}-rpm-low-${var.autoscaling_low_rpm_threshold}"
  alarm_description   = "Managed by Terraform"
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "RequestCountPerTarget"
  namespace           = "AWS/ApplicationELB"
  period              = "60"
  statistic           = "Average"
  threshold           = var.autoscaling_low_rpm_threshold

  dimensions = {
    ClusterName = var.cluster_name
    ServiceName = length(aws_ecs_service.service) > 0 ? lookup(aws_ecs_service.service[count.index], "name") : lookup(aws_ecs_service.service_with_discovery[count.index], "name")
  }

  alarm_actions = flatten([aws_appautoscaling_policy.service_autoscaling_down[count.index].arn])
}

resource "aws_cloudwatch_metric_alarm" "rpm_utilization_high" {
  count               = var.autoscaling_high_rpm_threshold == "-1" ? 0 : 1
  alarm_name          = "${var.cluster_name}-${var.environment}-${var.service_name}-${var.service_cluster}-rpm-high-${var.autoscaling_high_rpm_threshold}"
  alarm_description   = "Managed by Terraform"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "RequestCountPerTarget"
  namespace           = "AWS/ApplicationELB"
  period              = "60"
  statistic           = "Average"
  threshold           = var.autoscaling_high_rpm_threshold

  dimensions = {
    ClusterName = var.cluster_name
    ServiceName = length(aws_ecs_service.service) > 0 ? lookup(aws_ecs_service.service[count.index], "name") : lookup(aws_ecs_service.service_with_discovery[count.index], "name")
  }

  alarm_actions = flatten([aws_appautoscaling_policy.service_autoscaling_up[count.index].arn])
}
