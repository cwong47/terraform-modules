# AWS ECS Service Terraform module

Terraform module which creates an ECS service *without* Load Balancer.

## Usage

### ECS Fargate service with additional policies and EFS volume

```hcl
data "aws_caller_identity" "current" {}

variable "environment" {
  default     = "qa"
}

module "service_bastion_01" {
  source               = "../../modules/ecs-service"
  platform_version     = "1.4.0"
  account_id           = data.aws_caller_identity.current.account_id
  service_name         = "bastion"
  service_cluster      = "01"
  container_definition = file("../global/task-definitions/bastion.json")
  environment          = var.environment
  cluster_name         = module.cluster_default_01.name
  cluster_arn          = module.cluster_default_01.id
  vpc_id               = data.aws_vpc.usw2.id
  subnet_ids           = [data.aws_subnet_ids.usw2_public.ids]
  allowed_cidr         = [data.aws_vpc.usw2.cidr_block]
  ingress_from_port    = 22
  ingress_to_port      = 22
  assign_public_ip     = true
  container_name       = "${var.environment}-bastion-01"
  container_port       = 22
  image_name           = "lscr.io/linuxserver/openssh-server"
  cpu                  = "256"
  memory               = "512"
  desired_count        = 1

  iam_policy_arn = [
    "arn:aws:iam::aws:policy/ReadOnlyAccess",
    aws_iam_policy.bastion_rw.arn
  ]

  volume = [
    {
      name = "efs",
      efs_volume_configuration = [
        {
          "file_system_id" = module.efs.id,
          authorization_config = [
            {
              "access_point_id" = module.efs.access_point_ids["bastion"]
            }
          ]
        }
      ]
    }
  ]
}

data "aws_iam_policy_document" "bastion_rw" {
  statement {
    actions = [
      "ecs:UpdateService",
    ]

    resources = [
      "arn:aws:ecs:${var.region}:${data.aws_caller_identity.current.account_id}:service/${var.environment}-default-01/${var.environment}-bastion-01",
    ]
  }

  statement {
    actions = [
      "route53:ChangeResourceRecordSets",
    ]

    resources = [
      data.aws_route53_zone.company_public.arn,
    ]
  }
}

resource "aws_iam_policy" "bastion_rw" {
  name   = "${var.environment}-${var.region}-bastion-rw"
  policy = data.aws_iam_policy_document.bastion_rw.json
}
```

## Resources

| Name | Type |
|------|------|
| [aws_appautoscaling_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/appautoscaling_policy) | resource |
| [aws_appautoscaling_target](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/appautoscaling_target) | resource |
| [aws_cloudwatch_log_group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | resource |
| [aws_cloudwatch_metric_alarm](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_metric_alarm) | resource |
| [aws_ecs_service](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_service) | resource |
| [aws_ecs_task_definition](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_task_definition) | resource |
| [aws_iam_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | resource |
| [aws_iam_role_policy_attachment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_lb_listener_rule](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb_listener_rule) | resource |
| [aws_lb_target_group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb_target_group) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| `account_id` | AWS Account ID. | `string` | `null` | yes |
| `service_name` | The name of the service. | `string` | `null` | yes |
| `service_cluster` | The cluster name of the service. | `string` | `null` | yes |
| `region` | Default region. | `string` | `us-west-2` | no |
| `environment` | Account environment. | `string` | `null` | yes |
| `log_driver` | Default log driver to use. | `string` | `awsfirelens` | no |
| `canary` | Specify optional canary environment variable to ECS task definition. | `string` | `null` | no |
| `image_name` | Docker container image name. | `string` | `null` | yes |
| `image_tag` | Docker tag for images retrieved from ECR or docker registry. | `string` | `latest` | no |
| `ingress_from_port` | Standard TCP listening port from. | `number` | `80` | no |
| `ingress_to_port` | Standard TCP listening port to. | `number` | `80` | no |
| `desired_count` | The number of instances of the task definition to place and keep running. | `string` | `null` | yes |
| `launch_type` | The launch type on which to run your service. The valid values are EC2 and FARGATE. | `string` | `FARGATE` | no |
| `platform_version` | The version of FARGATE. | `string` | `LATEST` | no |
| `cluster_name` | Name of the ECS cluster to run the service in. | `string` | `null` | yes |
| `cluster_arn` | ARN of the ECS cluster to run the service in. | `string` | `null` | yes |
| `exec_command` | Manage ECS Execute Command. | `bool` | `true` | no |
| `deployment_minimum_healthy_percent` | The lower limit (as a percentage of the service's desiredCount) of the number of running tasks that must remain running and healthy in a service during a deployment. | `number` | `100` | no |
| `deployment_maximum_percent` | The upper limit (as a percentage of the service's desiredCount) of the number of running tasks that can be running in a service during a deployment. | `number` | `200` | no |
| `health_check_grace_period_seconds` | Seconds to ignore failing load balancer health checks on newly instantiated tasks to prevent premature shutdown. | `number` | `0` | no |
| `container_name` | The name of the container to associate with the load balancer (as it appears in a container definition). | `string` | `null` | yes |
| `container_port` | The port on the container to associate with the load balancer. | `number` | `null` | yes |
| `vpc_id` | VPC ID. | `string` | `null` | yes |
| `subnet_ids` | The subnets associated with the service. | `list(any)` | `null` | yes |
| `allowed_cidr` | The CIDR blocks account_id with the service. | `list(any)` | `null` | yes |
| `allowed_security_groups` | The security group ids that should be allowed unfiltered ingress to container instances. | `list(string)` | `[]` | no |
| `assign_public_ip` | Assign a public IP address to the ENI. | `bool` | `false` | no |
| `tags` | A map of tags to assign to the resource. | `map(any)` | `{}` | no |
| `deregistration_delay` | The amount of time in seconds to drain requests when de-registering a target. | `number` | `60` | no |
| `autoscaling_min_count` | The minimum number of instances of the task definition to place and keep running. | `number` | `-1` | no |
| `autoscaling_max_count` | The maximum number of instances of the task definition to place and keep running. | `number` | `-1` | no |
| `autoscaling_aggregation_type` | The aggregation type for the policy's metrics. | `string` | `Average` | no |
| `autoscaling_cooldown_down` | The amount of time, in seconds, after a scaling activity completes and before the next scaling activity can start. | `number` | `180` | no |
| `autoscaling_interval_upper_bound_down` | The upper bound for the difference between the alarm threshold and the CloudWatch metric. | `number` | `0` | no |
| `autoscaling_adjustment_down` | The number of members by which to scale, when the adjustment bounds are breached. A positive value scales up. A negative value scales down. | `number` | `-1` | no |
| `autoscaling_cooldown_up` | The amount of time, in seconds, after a scaling activity completes and before the next scaling activity can start. | `number` | `60` | no |
| `autoscaling_interval_upper_bound_up` | The upper bound for the difference between the alarm threshold and the CloudWatch metric. | `number` | `0` | no |
| `autoscaling_adjustment_up` | The number of members by which to scale, when the adjustment bounds are breached. A positive value scales up. A negative value scales down. | `number` | `1` | no |
| `autoscaling_low_cpu_threshold` | The CPU percent utilization threshold to autoscale down. | `number` | `-1` | no |
| `autoscaling_high_cpu_threshold` | The CPU percent utilization threshold to autoscale up. | `number` | `-1` | no |
| `autoscaling_low_memory_threshold` | The Memory percent utilization threshold to autoscale down. | `number` | `-1` | no |
| `autoscaling_high_memory_threshold` | The Memory percent utilization threshold to autoscale up. | `number` | `-1` | no |
| `autoscaling_low_rpm_threshold` | The RPM percent utilization threshold to autoscale down. | `number` | `-1` | no |
| `autoscaling_high_rpm_threshold` | The RPM Memory percent utilization threshold to autoscale up. | `number` | `-1` | no |
| `container_definition` | A container definition provided as a single valid JSON document. | `string` | `null` | no |
| `task_role_name` | The name of IAM role that allows your Amazon ECS container task to make calls to other AWS services. | `string` | `null` | no |
| `task_role_arn` | The ARN of IAM role that allows your Amazon ECS container task to make calls to other AWS services. | `string` | `null` | no |
| `iam_policy_arn` | IAM Policy to be attached to role. | `list(any)` | `["arn:aws:iam::aws:policy/ReadOnlyAccess"]` | no |
| `network_mode` | The Docker networking mode to use for the containers in the task. | `string` | `awsvpc` | no |
| `cpu` | The number of cpu units used by the task. | `number` | `null` | yes |
| `memory` | The amount (in MiB) of memory used by the task. | `number` | `null` | yes |
| `requires_compatibilities` | A set of launch types required by the task. | `list(any)` | `["FARGATE"]` | no |
| `service_registry_arn` | ARN of the Service Registry. | `string` | `null` | no |
| `volume` | A set of volume blocks that containers in your task may use. | `any` | `[]` | no |

## Outputs

| Name | Description |
|------|-------------|
| `n/a` | n/a |
