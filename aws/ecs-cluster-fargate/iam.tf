data "aws_iam_policy_document" "iam_policy_document" {
  statement {
    actions = [
      "kms:GenerateDataKey"
    ]

    resources = [
      aws_kms_key.kms_key.arn
    ]
  }

  statement {
    actions = [
      "ecs:ExecuteCommand"
    ]

    resources = [
      "arn:aws:ecs:${var.region}:${var.account_id}:cluster/${aws_ecs_cluster.ecs_cluster.name}",
      "arn:aws:ecs:${var.region}:${var.account_id}:cluster/${aws_ecs_cluster.ecs_cluster.name}/*",
      "arn:aws:ecs:${var.region}:${var.account_id}:task/${aws_ecs_cluster.ecs_cluster.name}",
      "arn:aws:ecs:${var.region}:${var.account_id}:task/${aws_ecs_cluster.ecs_cluster.name}/*",
    ]

    condition {
      test     = "StringNotLike"
      variable = "ecs:container-name"

      values = [
        "${var.environment}-bastion-*"
      ]
    }
  }
}

resource "aws_iam_policy" "iam_policy" {
  name        = "${var.name}-ecs-execute-command"
  path        = "/"
  description = "ECS Execute Command"
  policy      = data.aws_iam_policy_document.iam_policy_document.json

  tags = var.tags
}
