resource "aws_cloudwatch_log_group" "cloudwatch_log_group" {
  name              = "/ecs/cluster/${var.name}"
  kms_key_id        = aws_kms_key.kms_key.arn
  retention_in_days = var.log_group_retention

  tags = var.tags
}
