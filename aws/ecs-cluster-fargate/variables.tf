variable "account_id" {
}

variable "name" {
  description = "The name prepended to all aws resources provisioned."
}

variable "environment" {
}

variable "region" {
  default = "us-west-2"
}

variable "resource_type" {
  default = "aws"
}

variable "log_group_retention" {
  default = 30
}

variable "cloud_watch_encryption" {
  default = true
}

variable "tags" {
  type    = map(any)
  default = {}
}
