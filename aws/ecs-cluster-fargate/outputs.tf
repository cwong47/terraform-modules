output "name" {
  value = aws_ecs_cluster.ecs_cluster.name
}

output "id" {
  value = aws_ecs_cluster.ecs_cluster.id
}
