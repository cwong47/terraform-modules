# AWS ECS Cluster (Fargate) Terraform module

Terraform module which creates AWS ECS cluster resources for Fargate.

## Usage

### ECS Cluster for Fargate services

```hcl
data "aws_caller_identity" "current" {}

variable "environment" {
  default     = "qa"
}

variable "resource_type" {
  default     = "aws"
}

module "fargate" {
  source        = "../../modules/ecs-cluster-fargate"
  name          = "${var.environment}-cluster-01"
  account_id    = data.aws_caller_identity.current.account_id
  environment   = var.environment
  resource_type = "${var.resource_type}-ecs-cluster"
}
```

## Resources

| Name | Type |
|------|------|
| [aws_cloudwatch_log_group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | resource |
| [aws_ecs_cluster](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_cluster) | resource |
| [aws_iam_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_kms_alias](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_alias) | resource |
| [aws_kms_key](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_key) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| `name` | Cluster name. | `string` | `null` | yes |
| `account_id` | AWS Account ID. | `string` | `null` | yes |
| `environment` | Account environment. | `string` | `null` | yes |
| `region` | Default region. | `string` | `null` | no |
| `resource_type` | AWS resource type. | `string` | `aws` | no |
| `log_group_retention` | Cloudwatch log retention time. | `number` | `30` | no |
| `cloud_watch_encryption` | Encrypt Cloudwatch log. | `bool` | `true` | no |
| `tags` | A map of tags to assign to the resource. | `map(any)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| `name` | Cluster name. |
| `id` | Cluster ID. |
