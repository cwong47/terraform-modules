data "aws_iam_policy_document" "iam_policy_document_kms_key" {
  statement {
    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${var.account_id}:root"]
    }

    actions = ["kms:*"]

    resources = ["*"]
  }

  statement {
    principals {
      type        = "Service"
      identifiers = ["logs.${var.region}.amazonaws.com"]
    }

    actions = [
      "kms:Encrypt*",
      "kms:Decrypt*",
      "kms:ReEncrypt*",
      "kms:GenerateDataKey*",
      "kms:Describe*"
    ]

    resources = ["*"]
  }
}

resource "aws_kms_key" "kms_key" {
  description             = "ECS Cluster Key - ${var.name}"
  deletion_window_in_days = 7
  policy                  = data.aws_iam_policy_document.iam_policy_document_kms_key.json

  tags = var.tags
}

resource "aws_kms_alias" "kms_alias" {
  name          = "alias/${var.name}-key"
  target_key_id = aws_kms_key.kms_key.key_id
}
